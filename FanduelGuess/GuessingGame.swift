//
//  GuessingGame.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//
//  The basic requirememt of a guessing game.

import Foundation

protocol GuessingGame {

    // The number of correct guesses, changes as guesses are made
    var correctGuesses: Int { get }
    
    // The number of incorrect guesses, changes as guesses are made
    var incorrectGuesses: Int { get }
}
