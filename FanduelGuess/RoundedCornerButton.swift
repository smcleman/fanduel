//
//  RoundedCornerButton.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 19/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import UIKit

class RoundedCornerButton: UIButton {
    override func awakeFromNib() {
        layer.cornerRadius = 4
    }
}
