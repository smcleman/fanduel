//
//  Player.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 17/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//
//  Represents a simple player object.
//  Conforms to FPPGComparable by specifying an fppg property that wil be used for comparison
//

import Foundation
import SwiftyJSON

struct Player: FPPGComparable, Identifiable {
    let id: String
    let firstName: String
    let lastName: String
    let fppg: Double
    var fullName: String {
        return "\(firstName) \(lastName)"
    }
    
    let imageURL: String?
    
    /**
     
     Player initialiser
     
     - parameters:
        - json: A SwiftJSON object containing the required properties used to initialise a Player
     
     */
    init(withJSON json:JSON) {
        id = json["id"].stringValue
        firstName = json["first_name"].stringValue
        lastName = json["last_name"].stringValue
        fppg = json["fppg"].doubleValue
        imageURL = json["images"]["default"]["url"].string
    }
}
