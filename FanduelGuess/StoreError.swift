//
//  StoreError.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 20/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import Foundation

enum StoreError: Error {
    case noConnection
    case malformedResponse
    case unknown
}

extension StoreError: CustomStringConvertible {
    var description: String {
        switch self {
        case .noConnection:
            return "Network connection error"
        case .malformedResponse:
            return "Malformed JSON reponse"
        case .unknown:
            return "An unknown error occurred"
        }
    }
}
