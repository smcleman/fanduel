//
//  PlayerStore.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//
//  Responsible for fetching player data and returning instatiated Player objects.
//  In this case it is from an online json file. Could be from a REST API or on device database

import Foundation
import Alamofire
import SwiftyJSON

class PlayerStore {
    static let dataUrl = "https://cdn.rawgit.com/liamjdouglas/bb40ee8721f1a9313c22c6ea0851a105/raw/6b6fc89d55ebe4d9b05c1469349af33651d7e7f1/Player.json"
    
    static func fetchAll(_ completion: @escaping (_ players: [Player]?, _ error: StoreError?) -> Void) {
        Alamofire.request(dataUrl, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                PlayerStore.createPlayers(fromJSON: JSON(value), completion: completion)
            case .failure(let error):
                if let err = error as? URLError {
                    if err.code == URLError.notConnectedToInternet {
                        print("Mot connected to internet")
                        return completion(nil, .noConnection)
                    }
                } else {
                    return completion(nil, .unknown)
                }
                
            }
        }
    }
    
    static func createPlayers(fromJSON json: JSON, completion: (_ players: [Player]?, _ error: StoreError?) -> Void) {
        var players = [Player]()

        // Return an empty array of players if no "players" array was found in the json response
        guard let playersJSON = json["players"].array else {
            print("No players array found in json response")
            return completion(nil, .malformedResponse)
        }
        
        // iterate through each block of player json and create a Player model object, appending to our players array
        for playerJSON in playersJSON {
            let player = Player(withJSON: playerJSON)
            players.append(player)
        }
        
        completion(players, nil)
    }
    
    // Other potential methods could fetch players by id, from a paginated range, or with search filtering criteria
}
