//
//  HighLowGuessingGame.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//
//  Details the basic requirements of a High/Low guessing game.

import Foundation

protocol HighLowGuessingGame: GuessingGame {
    func guess<T: Comparable>(_ guess: T, isHigherThan others: [T]) -> Bool
    func guess<T: Comparable>(_ guess: T, isLowerThan others: [T]) -> Bool
}

extension HighLowGuessingGame {
    func guess<T: Comparable>(_ guess: T, isHigherThan others: [T]) -> Bool {
        guard let max = others.max() else {
            return false
        }
        
        return guess > max
    }
    
    func guess<T: Comparable>(_ guess: T, isLowerThan others: [T]) -> Bool {
        guard let min = others.min() else {
            return false
        }
        
        return guess < min
    }
}
