//
//  AudioPlayer.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 19/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import Foundation
import AVFoundation

enum GameSound: String {
    case correctGuess = "correct"
    case incorrectGuess = "wrong"
}

class AudioPlayer {
    var audioPlayer = AVAudioPlayer()
    
    init() {
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
    }
    
    func play(_ sound: GameSound) {
        guard let url = Bundle.main.url(forResource: sound.rawValue, withExtension: "wav") else {
            return
        }
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        } catch let error {
            print("Failed to play audio - \(error)")
        }
    }
    
}
