//
//  PlayerCollectionViewCell.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//
//  Cell to display player information.
//

import UIKit
import SDWebImage

class PlayerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var fppg: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundView?.layer.cornerRadius = 4
        selectedBackgroundView?.layer.cornerRadius = 4
        
        imageView.backgroundColor = UIColor.white
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 2
        imageView.layer.cornerRadius = 2.0
        imageView.layer.shadowColor = UIColor.black.cgColor
        imageView.layer.shadowOpacity = 0.4
        imageView.layer.shadowOffset = CGSize(width: 2, height: 2)
        
        fppg.isHidden = true
    }

    override func prepareForReuse() {
        self.isSelected = false
//        backgroundView?.backgroundColor = UIColor(red: 20.0/255.0, green: 142.0/255.0, blue: 246.0/255.0, alpha: 1)
        backgroundView?.backgroundColor = .clear
        imageView.sd_cancelCurrentImageLoad()
        imageView.image = nil
        name.text = ""
        fppg.text = ""
        fppg.isHidden = true;
    }
    
    // For simlicity in this small example we pass the Player model object to the cell to set it's self up/
    // In a larger app with more generic cells and greater reuse we wouldn't want to pass a concrete model in this way
    var player: Player? {
        didSet {
            name.text = player?.fullName
            if let fppgValue = player?.fppg {
                fppg.text = "\(fppgValue)"
            }
            if let imageURL = player?.imageURL {
                imageView.sd_setShowActivityIndicatorView(true)
                imageView.sd_setIndicatorStyle(.gray)
                imageView.sd_setImage(with: URL(string: imageURL))
            }
        }
    }
}
