//
//  Identifiable.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//
//  Defines that a conforming object can be uniquely identified by it's id property

import Foundation

protocol Identifiable: Comparable {
    var id: String { get }
}

// Implement default behaviour for a conforming object by implementing equatable method on the id property
extension Identifiable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
}
