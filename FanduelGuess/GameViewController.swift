//
//  GameViewController.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import UIKit
import AVFoundation

class GameViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var guessButton: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    
    let collectionViewCellReuseIdentifier = "PlayerCell"
    
    // The size of the pool to guess from in each guessing round
    let poolSizePerRound = 2
    
    // The target score required to win the game
    let targetScore = 10
    
    // The game instance
    var fppgGuessingGame: FPPGGuessingGame?
    
    // Collection view datasource array
    var datasource = [Player]()
    
    var audioPlayer = AudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register our custom Player collection view cell
        let nib = UINib(nibName: "PlayerCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: collectionViewCellReuseIdentifier)

        // Fetch Players from the store and pass through to initialise the game
        PlayerStore.fetchAll { (players, error) in
            guard let players = players, error == nil else {
                return self.showErrorAlertView(withMessage: error!.description)
            }
            
            self.initGame(withPlayerPool: players)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let isLandscape = UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation)
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = isLandscape ? .horizontal : .vertical
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        let isLandscape = UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation)
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = !isLandscape ? .horizontal : .vertical
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func showErrorAlertView(withMessage message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)

        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            let _ = self.navigationController?.popViewController(animated: true)
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Game related logic

extension GameViewController {
    /**
 
     Initialise a new FPPGGuessingGame setting the delegate to this view controller
     Then starts a new guessing round
     
     - parameters:
        - pool: The pool of Player objects on which to base the guessing game
    
    */
    func initGame(withPlayerPool pool: [Player]) {
        fppgGuessingGame = FPPGGuessingGame(withPlayerPool: pool, poolSizePerRound: poolSizePerRound, targetGuesses: targetScore)
        fppgGuessingGame?.delegate = self
        
        fppgGuessingGame?.nextGuessingRound()
        
        updateScore()
    }
    
    /**
     
     Updates the score UI
     
     */
    func updateScore() {
        scoreLabel.text = "\(fppgGuessingGame!.correctGuesses)"
    }
    
    /**
     
     Updates the UI based on the state of the last guess
     
     - parameters:
        - guess: The guessed Player
        - correct: Whether the guess was correct
     
     */
    func updateUI(forGuess guess: Player, correct: Bool) {
        updateScore()
        
        for cell in collectionView.visibleCells {
            if let cell = cell as? PlayerCollectionViewCell {
                cell.fppg.isHidden = false;
            }
        }
        
        if let index = datasource.index(of: guess) {
            let indexPath = IndexPath(item: index, section: 0)
            collectionView.deselectItem(at: indexPath, animated: false)
            if let cell = collectionView.cellForItem(at: indexPath) as? PlayerCollectionViewCell {
                cell.backgroundView?.backgroundColor = correct ? .green : .red
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.enableUI(true)
            self.fppgGuessingGame?.nextGuessingRound()
        }
    }
    
    /**
     
     Sets the state of interactive UI
     
     - parameters:
        - bool: Sets the enabled or disabled state
     
     */
    func enableUI(_ bool: Bool) {
        guessButton.isEnabled = bool
        guessButton.isHidden = !bool
        collectionView.isUserInteractionEnabled = bool;
    }
    
    func endGame() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "GameEndViewController") as? GameEndViewController {
            vc.score = fppgGuessingGame?.correctGuesses
            vc.willDismiss = {
                let _ = self.navigationController?.popViewController(animated: false)
            }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
}

// MARK: - IBActions

extension GameViewController {
    @IBAction func didGuess(_ sender: Any) {
        if let selectedIndexPath = collectionView.indexPathsForSelectedItems?.first {
            let guessedPlayer = datasource[selectedIndexPath.item]
            fppgGuessingGame?.guessIsHigher(guessedPlayer)
            enableUI(false)
        }
    }
}

// MARK: - FPPGGuessingGameDelegate

extension GameViewController: FPPGGuessingGameDelegate {
    func game(_ game: FPPGGuessingGame, didStartGuessingRoundWithPool pool: [Player]) {
        datasource = pool
        collectionView.reloadSections(IndexSet(integer: 0))
    }
    
    func game(_ game: FPPGGuessingGame, didGuessCorrectlyWithGuess guess: Player) {
        audioPlayer.play(.correctGuess)
        updateUI(forGuess: guess, correct: true)
    }
    
    func game(_ game: FPPGGuessingGame, didGuessIncorrectlyWithGuess guess: Player) {
        audioPlayer.play(.incorrectGuess)
        updateUI(forGuess: guess, correct: false)
    }
    
    func gameDidEnd(_ game: FPPGGuessingGame) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
             self.endGame()
        }
    }
}

// MARK: - UICollectionViewDataSource methods

extension GameViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellReuseIdentifier, for: indexPath as IndexPath) as! PlayerCollectionViewCell
        
        cell.player = datasource[indexPath.item]
        return cell
    }
}

// MARK: - UICollectionViewDelegate methods

extension GameViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.selectedBackgroundView?.alpha = 0
        UIView.animate(withDuration: 0.25) {
            cell?.selectedBackgroundView?.alpha = 1
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout methods

// TODO: The values here are not accuratley calculated. More work required to create an adaptable layout that can account for a variable number of cells.
extension GameViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        var height: CGFloat = 0.0
        
        if UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) {
            height = collectionView.bounds.size.height
        } else {
            height = collectionView.bounds.size.height / CGFloat(2) - (layout.minimumLineSpacing * CGFloat(2 - 1))
        }
        
        return CGSize(width: height, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var edgeInsets: CGFloat = 0.0

        if UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) {
            edgeInsets = 150
        } else {
            edgeInsets = 20
        }

        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 50
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
