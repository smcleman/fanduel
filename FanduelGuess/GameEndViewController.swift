//
//  GameEndViewController.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 19/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import UIKit

class GameEndViewController: UIViewController {
    @IBOutlet weak var messageLabel: UILabel!

    var willDismiss: (() -> ())?
    var didDismiss: (() -> ())?
    
    var score: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let score = score {
            messageLabel.text = "You guessed \(score) correctly"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func finish(_ sender: Any) {
        self.willDismiss?()
        self.dismiss(animated: true, completion: nil)
        self.didDismiss?()
    }
}
