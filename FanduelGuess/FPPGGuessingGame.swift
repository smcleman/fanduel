//
//  FPPGGuessingGame.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//
//  Conforms to the HighLowGuessingGame since we are comparing one comparable object against others.
//

import Foundation
import GameplayKit

class FPPGGuessingGame: HighLowGuessingGame {

    let playerPool: [Player]
    
    // The number of guesses required to complete the game
    let targetGuesses: Int
    
    // The size of the pool of players to guess from in the guessing round. Limiter will unsure this can never be greate than the player pool size.
    var poolSizePerRound: Int {
        didSet {
            if poolSizePerRound > playerPool.count {
                poolSizePerRound = playerPool.count
            }
        }
    }
    
    // Track the number of correct guesses
    internal private(set) var correctGuesses: Int = 0
    
    // Track the number of incorrect guesses
    internal private(set) var incorrectGuesses: Int = 0
    
    // The pool of players for the guessing round
    internal private(set) var roundPool = [Player]()
    
    var delegate: FPPGGuessingGameDelegate?
    
    /**
     
     Default initialiser for the game
     
     - parameters:
        - pool: The pool of Players on which the guessing game will be based.
        - poolSizePerRound: For each round, the size of the pool of Players from which to guess from
        - targetGuesses: The number of guesses required to complete the game
     
     */
    required init(withPlayerPool pool: [Player], poolSizePerRound: Int, targetGuesses: Int) {
        self.playerPool = pool
        self.poolSizePerRound = poolSizePerRound > playerPool.count ? playerPool.count : poolSizePerRound
        self.targetGuesses = targetGuesses
    }
    
    /**
     
     Starts a new guessing round by preparing the required pool of data to be guessed from.
     
     */
    func nextGuessingRound() {
        // A little weird using GameplayKit here but is has a built in array ahuffle method
        let shuffledPlayers = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: playerPool) as! [Player]
        roundPool = Array(shuffledPlayers[0...(poolSizePerRound - 1)])
        
        delegate?.game(self, didStartGuessingRoundWithPool: roundPool)
    }
    
    /** 
     
     Makes a guess that the given Player is greater than the current round's pool of Players
     
     - parameters:
        - player: The player being guessed
     
     */
    func guessIsHigher(_ player: Player) {
        // remove our selected player from the pool to compare against
        let remainingPool = roundPool.filter {$0 != player}
        let result = guess(player, isHigherThan: remainingPool)
        handleResult(result, forGuess: player)
    }
    
    /**
     
     Makes a guess that the given Player is lower than the current round's pool of Players
     
     - parameters:
        - player: The player being guessed
     
     */
    func guessIsLower(_ player: Player) {
        // remove our selected player from the pool to compare against
        let remainingPool = roundPool.filter {$0 != player}
        let result = guess(player, isLowerThan: remainingPool)
        handleResult(result, forGuess: player)
    }
    
    /**
     
     Handles the wht to do on the basis of a guess result
     
     - parameters:
        - result: Whether the guess was true of false
        - guess: The guessed player
     
     */
    private func handleResult(_ result: Bool, forGuess guess: Player) {
        if result {
            correctGuesses += 1
            delegate?.game(self, didGuessCorrectlyWithGuess: guess)
        } else {
            incorrectGuesses += 1
            delegate?.game(self, didGuessIncorrectlyWithGuess: guess)
        }
        
        if correctGuesses == targetGuesses {
            delegate?.gameDidEnd(self)
        }
    }
}

protocol FPPGGuessingGameDelegate {
    func game(_ game: FPPGGuessingGame, didStartGuessingRoundWithPool pool: [Player])
    func game(_ game: FPPGGuessingGame, didGuessCorrectlyWithGuess guess: Player)
    func game(_ game: FPPGGuessingGame, didGuessIncorrectlyWithGuess guess: Player)
    func gameDidEnd(_ game: FPPGGuessingGame)
}
