//
//  FPPGComparable.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 17/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//
//  Declares that an adopter can be compared by an fppg property
//

import Foundation

protocol FPPGComparable: Comparable {
    var fppg: Double { get }
}

// Implement default behaviour for a conforming object by implementing comparison methods on the fppg property
extension FPPGComparable {
    static func < (lhs: Self, rhs: Self) -> Bool {
        return lhs.fppg < rhs.fppg
    }
}
