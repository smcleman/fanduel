# FPPG Guessing Game

This is a relatively simple approach to a comparison based guessing game.
It makes use of 3rd party libraries via Cocoapods - Alamofire, SwiftyJSON and SDWebImage - to do the heavy lifting of dealing with web requests, JSON parsing and async image download and caching. There isn't much point reinventing those functions nowadays.

The game is set up with a pool of all players returned from the web request.
Each guessing round then selects a number of players from the total pool at random to form the current round's guessing pool. 

By default the guessing pool size is set to 2, so each round will be a choice between 2 players.
This can be changed by modifying the `poolSizePerRound` variable in `GameViewController`. This is passed through to the `FPPGGuessingGame` object to create the game session.

A UICollectionView is used to display PlayerCollectionViewCells that visually represent the players.
UICollectionView styling and layouts can become a huge project on their own so the implementation here is very simple. 
In theory you would be able to expand on this to provide a very adaptable game with multiple players to guess from, potentially changing from round to round.

