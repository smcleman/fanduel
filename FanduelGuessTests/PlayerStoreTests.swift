//
//  PlayerStoreTests.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import XCTest

@testable import FanduelGuess

class PlayerStoreTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testPlayerStoreWithValidJSON() {
        let playersJSON = TestUtils.loadJSON("players")
        PlayerStore.createPlayers(fromJSON: playersJSON) { (players) in
            XCTAssert(players.count == 2)
            
            XCTAssert(players[0].fullName == "Kevon Looney")
            XCTAssert(players[1].fullName == "Stephen Curry")
        }
    }
    
    func testPlayerStoreWithInValidJSON() {
        let playersJSON = TestUtils.loadJSON("lowScorePlayer")
        PlayerStore.createPlayers(fromJSON: playersJSON) { (players) in
            XCTAssert(players.count == 0)
        }
    }
    
}
