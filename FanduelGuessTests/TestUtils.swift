//
//  TestUtils.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 18/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import Foundation
import SwiftyJSON

class TestUtils {
    static func loadJSON(_ name: String) -> JSON {
        let bundle = Bundle(for: self)
        let path = bundle.path(forResource: name, ofType: "json")
        let url = URL(fileURLWithPath: path!)
        let data = try! Data.init(contentsOf: url)
        return JSON(data)
    }
}
