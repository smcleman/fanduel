//
//  PlayerTests.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 17/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import XCTest
import SwiftyJSON

@testable import FanduelGuess

class PlayerTests: XCTestCase {
    
    var lowPlayer: Player!
    var highPlayer: Player!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let lowFPPGJSON = TestUtils.loadJSON("lowScorePlayer")
        let highFPPGJSON = TestUtils.loadJSON("highScorePlayer")
        
        lowPlayer = Player(withJSON: lowFPPGJSON)
        highPlayer = Player(withJSON: highFPPGJSON)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        lowPlayer = nil
        highPlayer = nil
        
        super.tearDown()
    }
    
    func testPlayerInitialisation() {
        XCTAssertNotNil(lowPlayer.id)
        XCTAssertNotNil(lowPlayer.firstName)
        XCTAssertNotNil(lowPlayer.lastName)
        XCTAssertNotNil(lowPlayer.fullName)
        XCTAssertNotNil(lowPlayer.imageURL)
    }
    
    func testHighPlayerIsHigherThanLowPlayer() {
        XCTAssert(highPlayer > lowPlayer)
    }
    
    func testLowPlayerIsLowerThanHighPlayer() {
        XCTAssert(lowPlayer < highPlayer)
    }
    
    func testPlayerFullName() {
        XCTAssert(highPlayer.firstName == "Stephen")
        XCTAssert(highPlayer.lastName == "Curry")
        XCTAssert(highPlayer.fullName == "\(highPlayer.firstName) \(highPlayer.lastName)" )
    }
}
