//
//  FPPGGuessingGameTests.swift
//  FanduelGuess
//
//  Created by Stuart McLeman on 19/03/2017.
//  Copyright © 2017 Stuart McLeman. All rights reserved.
//

import XCTest
@testable import FanduelGuess

class FPPGGuessingGameTests: XCTestCase, FPPGGuessingGameDelegate {
    
    var lowPlayer: Player!
    var highPlayer: Player!
    
    var players: [Player]!
    var fppgGuessingGame: FPPGGuessingGame!
    
    var delegateNextRoundExpectaton: XCTestExpectation!
    var delegateGuessCorrectExpectaton: XCTestExpectation!
    var delegateGuessIncorrectExpectaton: XCTestExpectation!
    var delegateGameDidEndExpectaton: XCTestExpectation!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let lowFPPGJSON = TestUtils.loadJSON("lowScorePlayer")
        let highFPPGJSON = TestUtils.loadJSON("highScorePlayer")
        
        lowPlayer = Player(withJSON: lowFPPGJSON)
        highPlayer = Player(withJSON: highFPPGJSON)
        
        let playersJSON = TestUtils.loadJSON("players")
        PlayerStore.createPlayers(fromJSON: playersJSON) { (players) in
            self.players = players
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGameInstantiation() {
        fppgGuessingGame = FPPGGuessingGame(withPlayerPool: players, poolSizePerRound: 2, targetGuesses: 2)
        XCTAssert(fppgGuessingGame.correctGuesses == 0)
        XCTAssert(fppgGuessingGame.targetGuesses == 2)
        XCTAssert(fppgGuessingGame.poolSizePerRound == 2)
        XCTAssert(fppgGuessingGame.roundPool.count == 0)
    }
    
    func testGuessingRoundPoolSize() {
        fppgGuessingGame = FPPGGuessingGame(withPlayerPool: players, poolSizePerRound: 1, targetGuesses: 2)
        fppgGuessingGame.nextGuessingRound()
        XCTAssert(fppgGuessingGame.roundPool.count == 1)
    }
    
    func testGuessingRoundPoolSizeLimiter() {
        fppgGuessingGame = FPPGGuessingGame(withPlayerPool: players, poolSizePerRound: 4, targetGuesses: 2)
        
        fppgGuessingGame.nextGuessingRound()
        XCTAssert(fppgGuessingGame.roundPool.count == 2)
        
        fppgGuessingGame.poolSizePerRound = 6
        fppgGuessingGame.nextGuessingRound()
        XCTAssert(fppgGuessingGame.roundPool.count == 2)
    }
    
    func testGuessingGameNextRoundDelegate() {
        fppgGuessingGame = FPPGGuessingGame(withPlayerPool: players, poolSizePerRound: 2, targetGuesses: 2)
        fppgGuessingGame.delegate = self
        
        delegateNextRoundExpectaton = self.expectation(description: "didStartGuessingRoundWithPool delegate called")
        fppgGuessingGame.nextGuessingRound()
        
        self.waitForExpectations(timeout: 1)
    }
    
    func testGuessingGameCorrectGuessDelegate() {
        fppgGuessingGame = FPPGGuessingGame(withPlayerPool: players, poolSizePerRound: 2, targetGuesses: 2)
        fppgGuessingGame.delegate = self
        
        delegateGuessCorrectExpectaton = self.expectation(description: "didGuessCorrectlyWithGuess delegate called")
        fppgGuessingGame.nextGuessingRound()
        fppgGuessingGame.guessIsHigher(highPlayer)
        
        self.waitForExpectations(timeout: 1) { (error) in
            XCTAssert(self.fppgGuessingGame.correctGuesses == 1)
            XCTAssert(self.fppgGuessingGame.incorrectGuesses == 0)
        }
    }
    
    func testGuessingGameIncorrectGuessDelegate() {
        fppgGuessingGame = FPPGGuessingGame(withPlayerPool: players, poolSizePerRound: 2, targetGuesses: 2)
        fppgGuessingGame.delegate = self
        
        delegateGuessIncorrectExpectaton = self.expectation(description: "didGuessIncorrectlyWithGuess delegate called")
        fppgGuessingGame.nextGuessingRound()
        fppgGuessingGame.guessIsHigher(lowPlayer)
        
        self.waitForExpectations(timeout: 1) { (error) in
            XCTAssert(self.fppgGuessingGame.correctGuesses == 0)
            XCTAssert(self.fppgGuessingGame.incorrectGuesses == 1)
        }
    }
    
    func testGuessingGameDidEndDelegate() {
        fppgGuessingGame = FPPGGuessingGame(withPlayerPool: players, poolSizePerRound: 2, targetGuesses: 2)
        fppgGuessingGame.delegate = self
        
        delegateGameDidEndExpectaton = self.expectation(description: "gameDidEnd delegate called")
        
        fppgGuessingGame.nextGuessingRound()
        fppgGuessingGame.guessIsHigher(highPlayer)
        
        fppgGuessingGame.nextGuessingRound()
        fppgGuessingGame.guessIsHigher(highPlayer)
        
        self.waitForExpectations(timeout: 1) { (error) in
            XCTAssert(self.fppgGuessingGame.correctGuesses == 2)
            XCTAssert(self.fppgGuessingGame.incorrectGuesses == 0)
        }
    }
    
    func game(_ game: FPPGGuessingGame, didStartGuessingRoundWithPool pool: [Player]) {
        if (delegateNextRoundExpectaton != nil) {
            delegateNextRoundExpectaton.fulfill()
            XCTAssert(pool.count == game.poolSizePerRound)
        }
    }
    
    func game(_ game: FPPGGuessingGame, didGuessCorrectlyWithGuess guess: Player) {
        if (delegateGuessCorrectExpectaton != nil) {
            delegateGuessCorrectExpectaton.fulfill()
        }
    }
    
    func game(_ game: FPPGGuessingGame, didGuessIncorrectlyWithGuess guess: Player) {
        if (delegateGuessIncorrectExpectaton != nil) {
            delegateGuessIncorrectExpectaton.fulfill()
        }
    }
    
    func gameDidEnd(_ game: FPPGGuessingGame) {
        delegateGameDidEndExpectaton.fulfill()
    }
}
